# arduino-uxlib

A set of tools thanks to which your hardware will not piss off end users

## UxEncoder

* Handles rotary encoder properly.
* Uses `Encoder.h` under the hood.

Usage:

```
#include <UxEncoder.h>

#define encoderPinA 2
#define encoderPinB 3

UxEncoder enc(encoderPinA, encoderPinB);

void setup() {
    Serial.begin(9600);
}

void loop() {
    if(enc.changed()) {
        Serial.print("Encoder pos changed. Diff=");
        Serial.println(enc.getDiff());
    }
}
```

See `examples` for more, well..., examples.

## UxButton

* Handles switch/button properly
* Handles long press

Usage:

```
#include <UxButton.h>

#define buttonPin 2

UxButton btn(buttonPin);

void setup() {
    Serial.begin(9600);
}

void loop() {
    if(btn.clicked()) {
        Serial.print("Button clicked");
    }
}
```

See `examples` for more, well..., examples.

## Installation

```
cd ~/Arduino/libraries
git clone https://gitlab.com/marcinjn/arduino-uxlib.git UxLib
```
