#ifndef UX_BUTTON_H
#define UX_BUTTON_H

#define UX_BUTTON_LONG_PRESS_REPEAT_DELAY 100
#define UX_BUTTON_LONG_PRESS_TIME 500
#define UX_BUTTON_DEBOUNCE_TIME 100

class UxButton {
    public:
        UxButton(int pin);
        UxButton(int pin, int long_press_ms);
        bool clicked();
    private:
        int pin;
        unsigned long lastPress = 0;
        int long_press_ms = UX_BUTTON_LONG_PRESS_TIME;
        bool pressed = false;
};
#endif
