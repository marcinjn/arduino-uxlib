#include "UxEncoder.h"

UxEncoder::UxEncoder(int pinA, int pinB) {
    e = new Encoder(pinA, pinB);
}

UxEncoder::UxEncoder(int pinA, int pinB, int debounceTime) {
    e = new Encoder(pinA, pinB);
    this->debounceTime = debounceTime;
}

bool UxEncoder::changed() {
    currentRaw = e->read();
    current = ceil(currentRaw / 4.0);
    unsigned long t = millis();
    bool changed = false;

    if (lastChanged - t > 100) {
        lastChanged = t;
        diffRaw = 0;
        diff = 0;

        if (currentRaw != previousRaw) {
            if (abs(currentRaw - previousRaw) > 3) {
                diffRaw = currentRaw - previousRaw;
                previousRaw = currentRaw;
                if (current != previous) {
                    diff = current - previous;
                    previous = current;
                    changed = true;
                }
            }
        }
    }

    return changed;
}

int UxEncoder::getDiff() {
    return diff;
}

int UxEncoder::getValue() {
    return current;
}

int UxEncoder::getValueRaw() {
    return currentRaw;
}

int UxEncoder::getDiffRaw() {
    return diffRaw;
}

bool UxEncoder::left() {
    return diff < 0;
}

bool UxEncoder::right() {
    return diff > 0;
}
