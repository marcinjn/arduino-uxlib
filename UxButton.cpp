#include <Arduino.h>
#include "UxButton.h"


UxButton::UxButton(int pin) {
    this->pin = pin;
    this->long_press_ms = 500;
}

UxButton::UxButton(int pin, int long_press_ms) {
    this->pin = pin;
    this->long_press_ms = long_press_ms;
}

bool UxButton::clicked() {
    unsigned long delta = millis() - lastPress;
    bool btnState = digitalRead(pin);
    if (btnState && ((!pressed && delta > UX_BUTTON_DEBOUNCE_TIME) || (pressed && delta > long_press_ms))) {
        if (!pressed) {
            lastPress = millis();
        } else {
            lastPress = millis() - long_press_ms + UX_BUTTON_LONG_PRESS_REPEAT_DELAY;
        }
        pressed = true;
        return true;
    } else {
        if (pressed && !btnState && delta > UX_BUTTON_DEBOUNCE_TIME) {
            pressed = false;
        }
        return false;
    }
}
