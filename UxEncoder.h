#include <Encoder.h>

#ifndef UX_ENCODER_H
#define UX_ENCODER_H

class UxEncoder {
public:
  UxEncoder(int pinA, int pinB);
  UxEncoder(int pinA, int pinB, int debounceTime);
  bool changed();
  int getDiff();
  int getValue();
  int getValueRaw();
  int getDiffRaw();
  bool left();
  bool right();
private:
  int debounceTime = 100;
  long current = 0;
  long currentRaw = 0;
  long previous = 0;
  long previousRaw = 0;
  int diff = 0;
  int diffRaw = 0;
  unsigned long lastChanged = 0;
  Encoder *e;
};
#endif
