#include <UxEncoder.h>

#define encoderPinA 2
#define encoderPinB 3

UxEncoder enc(encoderPinA, encoderPinB);

void setup() {
    Serial.begin(9600);
}

void loop() {
    if(enc.changed()) {
        Serial.print("Encoder pos changed. Diff=");
        Serial.println(enc.getDiff());
    }
}
